Assignment 4
============
Space-based Computing: JavaSpaces

This assignment is a Auction application, consisting of Clients, Servers, 
and Agents. Clients can create and bid on auctioned items by talking to the
server which keeps a Space of the auction items. Clients can also buy 
items directly from other Clients through RMI (Remote Method Invocation).

The application is built using the Reactor, Acceptor, Connector, and Broker 
design patterns.

Testing
=======
All log messages will be found in "auctionhouse.log".


How to run:
==================================
1) Run the Broker
  - To change the number of Brokers, modify "broker.cfg"
  
2) Run Y number of Servers in Eclipse
  - Make sure that all servers have unique names.
  - In server.cfg, the AUCTION_HOUSE value should be set to edu.carleton.comp4104.assignment4.server.AuctionHouseFaultTolerant
  
3) Run the AuctionAgent
	- In auction-agent.cfg, the NUM_AGENTS value will set the amount of agents to create
	- needs to have VMArguments:
		-Djava.security.policy=policy.all
		-Djava.rmi.server.RMIClassLoaderSpi=net.jini.loader.pref.PreferredClassProvider
		-Djava.rmi.server.codebase=file:${workspace_loc}/Assignment4/bin/
		
4) Run the ServerBridge
	- needs to have VMArguments:
		-Djava.security.policy=policy.all
		-Djava.rmi.server.RMIClassLoaderSpi=net.jini.loader.pref.PreferredClassProvider
		
5) Run X number Client in Eclipse.
  - Make sure all Clients have unique Names
  - need to have VMArguments:
  		-Djava.security.policy=policy.all
		-Djava.rmi.server.codebase=file:${workspace_loc}/Assignment4/bin/
		
6) Run X number ItemAgent in Eclipse
	- need to provide command line arguments (-i2 is optional)
		-i1 <first item to look for> -i2 <second item to look for> -ri <recommended item>
	- needs to have VMArguments:
		-Djava.security.policy=policy.all
		-Djava.rmi.server.RMIClassLoaderSpi=net.jini.loader.pref.PreferredClassProvider
		

Optional:
	Broker can be run with option -c <config_file_path>
	server can be run with option -name <server_name> -c <config_file_path>
	client can be run with options -name <client_name> -c <config_file_path>

Debugging information is all output to the console for both the client and server.
package edu.carleton.comp4104.assignment4.client;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Item;

public class RMIHandlerImpl extends UnicastRemoteObject implements RMIHandler {
	private final Logger LOG = LoggerFactory.getLogger(ClientUpdateEventHandler.class);
	private static final long serialVersionUID = 1L;
	protected Registry registry;
	protected Client client;

	public RMIHandlerImpl(Client client) throws RemoteException {
		this.client = client;
	}

	/**
	 * A client buys an item owned by this client
	 */
	@Override
	public String buyNow(String itemName) {
		client.bid("remove", itemName);
		LOG.info(client.getServiceName()+": "+ itemName + " has been sold.");
		return itemName + " bought from " + client.getName();
	}

	/**
	 * A client asks for a description of an item owned by this client
	 */
	@Override
	public String getDescription(String itemName) {
		Item item = null;
		for (Item i : client.getItems()) {
			if (i.getItemName().equals(itemName)) {
				item = i;
			}
		}
		LOG.info(client.getServiceName()+": Sent description for " + itemName + ".");
		return item.toString();
	}
}

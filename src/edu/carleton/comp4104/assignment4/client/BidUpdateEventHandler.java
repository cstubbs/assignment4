package edu.carleton.comp4104.assignment4.client;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;
import edu.carleton.comp4104.assignment4.common.Item;

public class BidUpdateEventHandler implements EventHandler {
	private final Logger LOG = LoggerFactory.getLogger(BidUpdateEventHandler.class);
	@Override
	public boolean handleEvent(Event e) {

		// Get the connection/client that this Handler was called on
		Connection connection = (Connection) e.get("connection");
		Client client = (Client) connection.getService();

		LOG.info(client.getServiceName()+": BID UPDATE");

		// Update Client UI based on new items
		@SuppressWarnings("unchecked")
		ArrayList<Item> items = (ArrayList<Item>) e.get("items");

		client.setItems(items);
		client.updateClientUI();

		return false;
	}
}

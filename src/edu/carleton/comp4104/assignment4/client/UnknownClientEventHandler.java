package edu.carleton.comp4104.assignment4.client;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;

public class UnknownClientEventHandler implements EventHandler {

	@Override
	public boolean handleEvent(Event e) {

		// Get the connection/client that this Handler was called on
		Connection connection = (Connection) e.get("connection");
		Client client = (Client) connection.getService();

		client.displayError("Server Received an UNKNOWN Event");

		return true;
	}

}

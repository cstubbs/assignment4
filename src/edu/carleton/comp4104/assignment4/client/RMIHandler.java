package edu.carleton.comp4104.assignment4.client;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIHandler extends Remote {

	public String buyNow(String itemName) throws RemoteException;

	public String getDescription(String itemName) throws RemoteException;

}

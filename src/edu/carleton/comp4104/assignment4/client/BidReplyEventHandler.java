package edu.carleton.comp4104.assignment4.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;

public class BidReplyEventHandler implements EventHandler {
	private final Logger LOG = LoggerFactory.getLogger(BidReplyEventHandler.class);
	@Override
	public boolean handleEvent(Event e) {

		// Get the connection/client that this Handler was called on
		Connection connection = (Connection) e.get("connection");
		Client client = (Client) connection.getService();

		LOG.info(client.getServiceName()+": Bid Reply received");

		String itemName = (String) e.get("itemName");
		String response = (String) e.get("response");

		// Handle the response appropriately
		switch (response) {
		case "invalid_input":
			client.displayError("Invalid Input. Try again.");
			break;
		case "bid_too_low":
			client.displayError("Bid on " + itemName + " too low");
			break;
		case "item_exists":
			client.displayError(itemName + " already exists");
			break;
		case "no_item_selected_bid":
			client.displayError("Must select an item to make a bid");
			break;
		case "no_item_selected_remove":
			client.displayError("Must select an item to remove it");
			break;
		case "item_removed":
			client.displayError("Item " + itemName + " has been removed");
			break;
		case "not_your_item":
			client.displayError("Item " + itemName + " is not your auction item");
			break;
		default:
			client.displaySuccess(response);
			break;
		}

		return true;
	}
}

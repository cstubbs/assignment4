package edu.carleton.comp4104.assignment4.client;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Properties;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Connector;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.Item;
import edu.carleton.comp4104.assignment4.common.JMSConnector;
import edu.carleton.comp4104.assignment4.common.RMIRegistry;
import edu.carleton.comp4104.assignment4.common.Service;

public class Client extends Service {
	private static final Logger LOG = LoggerFactory.getLogger(Client.class);
	private static String DEFAULT_NAME = "Kai";
	private static String DEFAULT_cfgFILE = "src/edu/carleton/comp4104/assignment4/client/client.cfg";
	private Connector connector;
	private Connection connection;

	private Registry registry;
	private RMIHandler rmiHandler;

	// Auction House Variables
	private ClientUI clientUI;
	private String serviceName;
	private ArrayList<Item> items;
	private TreeSet<String> users;

	public Client(String serviceName, String eventsCfg, String brokerCfg) throws RemoteException {
		super(serviceName, eventsCfg);

		connector = new JMSConnector(this, brokerCfg);

		this.serviceName = serviceName;
		this.items = new ArrayList<Item>();
		this.users = new TreeSet<String>();

		this.registry = java.rmi.registry.LocateRegistry.getRegistry(RMIRegistry.PORT);
		rmiHandler = new RMIHandlerImpl(this);
		registry.rebind(serviceName, rmiHandler);

		clientUI = new ClientUI(this);
		clientUI.setVisible(true);
	}

	public static void main(String[] args) {

		// Default in case no args
		String name = DEFAULT_NAME;
		String cfgFile = DEFAULT_cfgFILE;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-name")) {
				name = args[++i];
			} else if (args[i].equals("-c")) {
				cfgFile = args[++i];
			}
		}

		// Create and Start Client
		try {
			Properties p = new Properties();
			p.load(new FileReader(cfgFile));

			String eventsCfg = p.getProperty("EVENTS_CFG");
			String brokerCfg = p.getProperty("BROKER_CFG");

			Client client = new Client(name, eventsCfg, brokerCfg);
			client.run();
		} catch (RemoteException e) {
			LOG.error(name+ ": Error connecting to regitry on port: " + RMIRegistry.PORT, e);
		} catch (FileNotFoundException e) {
			LOG.error("", e);
		} catch (IOException e) {
			LOG.error("", e);
		}
	}

	/**
	 * Run the Client. Create a connection to the server then login.
	 */
	public void run() {
		try {
			connection = connector.connect();
			Thread t = new Thread(connection);
			t.start();

			login();
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	/**
	 * Send a login Event to the Server.
	 */
	public void login() {
		try {
			Event e = new Event("LOGIN");
			e.put("id", serviceName);

			connection.sendEvent(e);
		} catch (Exception e) {
			LOG.error(serviceName+ ": Error Sending Event: ", e);
		}
	}

	/**
	 * Login failed. Ask Client for new name, if name is null call quit(), else
	 * call login().
	 */
	public void loginFailed(String reason) {
		serviceName = clientUI.getNewName(reason);
		if (serviceName == null) {
			quit();
		} else {
			updateClientUI();
			run();
		}
	}

	/**
	 * Send a logout Event to the Server.
	 */
	public void logout() {
		try {
			Event e = new Event("LOGOUT");
			e.put("id", serviceName);

			connection.sendEvent(e);
		} catch (Exception e) {
			LOG.error(serviceName+ ": Error Sending Event: ", e);
		}
	}

	/**
	 * Send a login Event to the Server.
	 */
	public void reconnect(String reason) {
		connection = null;
		LOG.error(serviceName+": "+ reason);
		if (clientUI.reconnect(reason)) {
			run();
		} else {
			System.exit(0);
		}
	}

	/**
	 * Send a bid Event to the Server.
	 */
	public String bid(String input, String itemName) {
		RMIHandler rmiHandler = null;
		String owner = "";
		
		if (input.split(" ")[0].equals("remove") && (input.split(" ").length == 1)) {
			for (Item i : items) {
				if (i.getItemName().equals(itemName))
					owner = i.getOwner();
			}
			if (owner.startsWith("Auction Agent ")) 
				return "This item cannot be removed!";
		}
		// Get description directly from the item owner
		if (input.split(" ")[0].equals("description") && (input.split(" ").length == 1 || input.split(" ").length == 2)) {
			if (input.split(" ").length > 1) {
				itemName = input.split(" ")[1];
			}
			for (Item i : items) {
				if (i.getItemName().equals(itemName))
					owner = i.getOwner();
			}
			if (owner.equals(""))
				return "invalid input";
			try {
				rmiHandler = (RMIHandler) registry.lookup(owner);
				clientUI.showDescription(rmiHandler.getDescription(itemName));
			} catch (RemoteException | NotBoundException e) {
				LOG.error(serviceName+ " Error connecting to remote client: " + owner + ".", e);
				return "";
			}
			LOG.info(serviceName, "Received " + itemName + " description from " + owner + ".");
		}
		// Buy the item directly from the item owner
		else if (input.split(" ")[0].equals("buynow") && (input.split(" ").length == 1 || input.split(" ").length == 2)) {
			if (input.split(" ").length > 1) {
				itemName = input.split(" ")[1];
			}

			for (Item i : items) {
				if (i.getItemName().equals(itemName))
					owner = i.getOwner();
			}
			if (owner.equals(""))
				return "invalid input";
			try {
				rmiHandler = (RMIHandler) registry.lookup(owner);
				LOG.info(serviceName+ ": Bought " + itemName + " from from " + owner + ".");
				return rmiHandler.buyNow(itemName);
			} catch (RemoteException | NotBoundException e) {
				LOG.error(serviceName+ ": Error connecting to remote client: " + owner + ".", e);
				return "";
			}
		}
		// send BID event to the server
		else {
			try {
				Event e = new Event("BID");
				e.put("id", serviceName);
				e.put("input", input);
				e.put("itemName", itemName);

				connection.sendEvent(e);
			} catch (Exception e) {
				LOG.error(serviceName+ " Error Sending Event: ", e);
			}
		}
		return "";
	}

	/**
	 * Quit the application. Stop the connection first.
	 */
	public void quit() {
		// Clean up connector/connection
		try {
			registry.unbind(serviceName);
			connection.stop();
			connector.stop();
			connection = null;
			connector = null;
		} catch (Exception e) {
			LOG.error("", e);
		}

		System.exit(0);
	}

	/**
	 * Update the Client UI
	 */
	public void updateClientUI() {
		clientUI.updateNameLabel();
		clientUI.updateItemModel();
		clientUI.updateUserModel();
	}

	public void displaySuccess(String message) {
		LOG.info(serviceName +": "+ message);
	}

	public void displayError(String reason) {
		clientUI.displayError(reason);
	}

	public String getName() {
		return serviceName;
	}

	public void setName(String name) {
		this.serviceName = name;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public TreeSet<String> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<String> users) {
		this.users = new TreeSet<String>(users);
	}
}

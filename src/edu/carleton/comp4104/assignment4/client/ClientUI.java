package edu.carleton.comp4104.assignment4.client;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DecimalFormat;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import edu.carleton.comp4104.assignment4.common.Item;

public class ClientUI extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Client client;

	// UI Variables
	static int X_SIZE = 540;
	static int Y_SIZE = 400;
	private JLabel nameLabel;
	private static JList<String> itemList;
	private static JList<String> userList;
	private DefaultListModel<String> itemModel;
	private DefaultListModel<String> userModel;
	private JTextField textField;
	private JButton bidButton;

	public ClientUI(Client client) {
		super("Auction House");

		this.client = client;

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setSize(X_SIZE, Y_SIZE);
		setLocation(300, 150);
		setResizable(false);
		setLayout(null);

		WindowListener exitListener = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				logout();
			}
		};
		addWindowListener(exitListener);

		nameLabel = new JLabel("User: " + client.getName());
		nameLabel.setBounds(20, 0, 500, 50);
		add(nameLabel);

		itemModel = new DefaultListModel<String>();
		for (Item item : client.getItems()) {
			itemModel.addElement(padString(item.getItemName(), 50) + "$" + item.getPrice());
		}

		itemList = new JList<String>(itemModel);
		itemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		itemList.setFont(new Font("Monospaced", Font.PLAIN, 14));
		JScrollPane itemPane = new JScrollPane(itemList);
		itemPane.setBounds(20, 50, 500, 140);
		add(itemPane);

		userModel = new DefaultListModel<String>();
		for (String s : client.getUsers()) {
			userModel.addElement(s);
		}

		userList = new JList<String>(userModel);
		userList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		userList.setFont(new Font("Monospaced", Font.PLAIN, 14));
		JScrollPane userPane = new JScrollPane(userList);
		userPane.setBounds(20, 205, 500, 100);
		add(userPane);

		textField = new JTextField();
		textField.setBounds(20, 320, 400, 30);
		textField.setFont(new Font("Monospaced", Font.PLAIN, 14));
		add(textField);

		bidButton = new JButton("Bid");
		bidButton.setBounds(440, 320, 80, 30);
		bidButton.addActionListener(this);
		add(bidButton);
	}

	/**
	 * A function to 'pretty' up the Items Name and Price.
	 * 
	 * @param s
	 * @param n
	 * @return
	 */
	public static String padString(String s, int n) {
		for (int i = s.length(); i < n; i++) {
			s += " ";
		}
		return s;
	}

	/**
	 * Update the itemModel.
	 */
	public void updateItemModel() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				itemModel.clear();
				for (Item item : client.getItems()) {
					DecimalFormat df = new DecimalFormat("#.00");
					float price = item.getPrice();
					itemModel.addElement(padString(item.getItemName(), 45) + "$" + df.format(price));
				}
			}
		});
	}

	/**
	 * Update the userModel.
	 */
	public void updateUserModel() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				userModel.clear();
				for (String s : client.getUsers()) {
					userModel.addElement(s);
				}
			}
		});
	}

	/**
	 * Update the nameLabel.
	 */
	public void updateNameLabel() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				nameLabel.setText("User: " + client.getName());
			}
		});
	}

	/**
	 * Returns a new name from a JOptionPane.
	 * 
	 * @param reason
	 * @return
	 */
	public String getNewName(String reason) {
		return JOptionPane.showInputDialog(ClientUI.this, "Error " + reason + "! Enter a new name : ", reason, JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Asks whether the user wants to logout.
	 */
	public void logout() {
		int reply = JOptionPane.showConfirmDialog(ClientUI.this, "Are you sure you want to logout?", "Logout", JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			client.logout();
		}
	}

	/**
	 * Asks whether the user wants to reconnect.
	 * 
	 * @param reason
	 * @return
	 */
	public boolean reconnect(String reason) {
		int reply = JOptionPane.showConfirmDialog(ClientUI.this, reason + "\nWould you like to reconnect?", "Connection Error", JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			return true;
		}
		return false;
	}

	/**
	 * Displays an error message to the Client.
	 * 
	 * @param reason
	 */
	public void displayError(String reason) {
		JOptionPane.showMessageDialog(ClientUI.this, reason, "Error", JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bidButton) {
			String itemName = (String) itemList.getSelectedValue();
			if (itemName != null)
				itemName = itemName.split(" ")[0];
			String result = client.bid(textField.getText().trim(), itemName);
			if (result.equals("invalid input")) {
				displayError("Invalid Input. Try again.");
				textField.setText("");
			} else if ((result.contains(" bought from ")) || (result.isEmpty())) {
				textField.setText(result);
			} else {
				displayError(result);
				textField.setText("");
			}	
		}
	}

	public void showDescription(String description) {
		JOptionPane.showMessageDialog(ClientUI.this, description);
	}

	public void setText(String response) {
		textField.setText(response);
	}
}

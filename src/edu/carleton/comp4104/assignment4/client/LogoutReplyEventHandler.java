package edu.carleton.comp4104.assignment4.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;

public class LogoutReplyEventHandler implements EventHandler {
	private final Logger LOG = LoggerFactory.getLogger(ClientUpdateEventHandler.class);
	@Override
	public boolean handleEvent(Event e) {

		// Get the connection/client that this Handler was called on
		Connection connection = (Connection) e.get("connection");
		Client client = (Client) connection.getService();

		LOG.info(client.getServiceName()+ ": Logout Reply received");

		// Tell Client to quit
		client.quit();

		return true;
	}
}

package edu.carleton.comp4104.assignment4.server;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import edu.carleton.comp4104.assignment4.common.Broker;
import edu.carleton.comp4104.assignment4.common.ConnectorFactory;
import edu.carleton.comp4104.assignment4.common.Item;
import edu.carleton.comp4104.assignment4.common.Marshaller;

public class AuctionHouseFaultTolerant implements AuctionHouse, MessageListener {
	private static final Logger LOG = LoggerFactory.getLogger(AuctionHouseFaultTolerant.class);
	private ConcurrentHashMap<String, Item> items;
	private ConcurrentLinkedQueue<String> users;

	private boolean online;
	public static String SERVER_TOPIC = "SERVERAUCTION";
	private ConnectionFactory factory;
	private javax.jms.Connection connection;
	private MessageConsumer topicConsumer;
	private MessageProducer topicProducer;
	private Session session;

	public AuctionHouseFaultTolerant() {
		this.items = new ConcurrentHashMap<String, Item>();
		this.users = new ConcurrentLinkedQueue<String>();
		this.online = false;
	}

	@Override
	public synchronized boolean addItem(Item newItem, String itemName) {
		if (!online)
			return false;
		if (items.containsKey(itemName))
			return false;
		else {
			items.put(itemName, newItem);
			return publishAddItem(newItem);
		}
	}

	@Override
	public synchronized boolean addUser(String name) {
		if (!online)
			return false;
		if (!users.contains(name)) {
			users.add(name);
			return publishAddUser(name);
		}
		return false;
	}

	public synchronized ConcurrentHashMap<String, Item> getItems() {
		return items;
	}

	public synchronized ConcurrentLinkedQueue<String> getUsers() {
		return users;
	}

	public void start() {
		try {
			factory = ConnectorFactory.getConnectionFactory(Broker
					.getBrokerString());
			connection = factory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
			// Create the topic of interest
			Topic topic = (Topic) session.createTopic(SERVER_TOPIC);
			topicConsumer = session.createConsumer((Destination) topic);
			topicProducer = session.createProducer((Destination) topic);
			topicConsumer.setMessageListener(this);
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (NamingException ne) {
			LOG.error("", ne);
		}
		online = true;
	}

	@Override
	public void stop() {
		try {
			connection.stop();
			session.close();
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		}
		online = false;
	}

	private boolean publishRemoveUser(String name) {
		MapMessage mm;
		try {
			mm = session.createMapMessage();
			mm.setInt("ID", hashCode());
			mm.setString("TYPE", "USER");
			mm.setString("USER", name);
			mm.setString("OPERATION", "REMOVE");
			topicProducer.send(mm);
			return true;
		} catch (JMSException e) {
			LOG.error("", e);
		}
		return false;
	}

	private boolean publishAddUser(String name) {
		MapMessage mm;
		try {
			mm = session.createMapMessage();
			mm.setInt("ID", hashCode());
			mm.setString("TYPE", "USER");
			mm.setString("USER", name);
			mm.setString("OPERATION", "ADD");
			topicProducer.send(mm);
			return true;
		} catch (JMSException e) {
			LOG.error("", e);
		}
		return false;
	}

	private boolean publishRemoveItem(String item) {
		MapMessage mm;
		try {
			mm = session.createMapMessage();
			mm.setInt("ID", hashCode());
			mm.setString("TYPE", "ITEM");
			mm.setString("ITEM", item);
			mm.setString("OPERATION", "REMOVE");
			topicProducer.send(mm);
			return true;
		} catch (JMSException e) {
			LOG.error("", e);
		}
		return false;
	}

	private boolean publishAddItem(Item item) {
		MapMessage mm;
		try {
			mm = session.createMapMessage();
			mm.setInt("ID", hashCode());
			mm.setString("TYPE", "ITEM");
			mm.setBytes("ITEM", Marshaller.serializeObject(item));
			mm.setString("OPERATION", "ADD");
			topicProducer.send(mm);
			return true;
		} catch (JMSException e) {
			LOG.error("", e);
		} catch (IOException e) {
			LOG.error("", e);
		}
		return false;
	}
	
	private boolean publishUpdateItem(Item item) {
		MapMessage mm;
		try {
			mm = session.createMapMessage();
			mm.setInt("ID", hashCode());
			mm.setString("TYPE", "ITEM");
			mm.setBytes("ITEM", Marshaller.serializeObject(item));
			mm.setString("OPERATION", "UPDATE");
			topicProducer.send(mm);
			return true;
		} catch (JMSException e) {
			LOG.error("", e);
		} catch (IOException e) {
			LOG.error("", e);
		}
		return false;
	}
	

	@Override
	public synchronized void onMessage(Message m) {
		try {
			if (m instanceof MapMessage) {
				MapMessage mm = (MapMessage) m;
				// Ignore my own updates
				// We have to do this because of the way
				// that the server processes updates.
				if (mm.getInt("ID") == hashCode())
					return;
				if (mm.getString("TYPE").equals("USER")) {
					if (mm.getString("OPERATION").equals("ADD")) {
						users.add(mm.getString("USER"));
					} else if (mm.getString("OPERATION").equals("REMOVE")) {
						users.remove(mm.getString("USER"));
					}
				} else if (mm.getString("TYPE").equals("ITEM")) {
					if (mm.getString("OPERATION").equals("ADD") || mm.getString("OPERATION").equals("UPDATE")) {
						Item item = (Item) Marshaller.deserializeObject(mm.getBytes("ITEM"));
						items.put(item.getItemName(), item);
					} else if (mm.getString("OPERATION").equals("REMOVE")) {
						items.remove(mm.getString("ITEM"));
					} 
				}
			}
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (IOException ioE) {
			LOG.error("", ioE);
		}
	}

	@Override
	public synchronized boolean removeItem(String itemName) {
		if (!online)
			return false;
		if (items.containsKey(itemName)) {
			items.remove(itemName);
			return publishRemoveItem(itemName);
		}
		return false;
	}

	@Override
	public synchronized boolean removeUser(String name) {
		if (!online)
			return false;
		if (users.contains(name)) {
			users.remove(name);
			return publishRemoveUser(name);
		}
		return false;
	}

	@Override
	public boolean updateItem(String itemName, float value) {
		if (!online)
			return false;
		Item item = items.get(itemName);
		if (item == null)
			return false;
		else if (item.getItemName().equals(itemName) && value > item.getPrice()) {
			item.setPrice(value);
			return publishUpdateItem(item);
		}
		return false;
	}
}

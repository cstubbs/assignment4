package edu.carleton.comp4104.assignment4.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;

public class UnknownServerEventHandler implements EventHandler {
	private final Logger LOG = LoggerFactory.getLogger(UnknownServerEventHandler.class);
	@Override
	public boolean handleEvent(Event e) {

		// Unknown Event Server
		Connection connection = (Connection) e.get("connection");

		String id = (String) e.get("id");
		LOG.warn(connection.getService().getServiceName()+": "+ id + " sent UNKNOWN event");

		return true;
	}

}

package edu.carleton.comp4104.assignment4.server;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;
import edu.carleton.comp4104.assignment4.common.Item;

public class LoginEventHandler implements EventHandler {
	private final Logger LOG = LoggerFactory.getLogger(LoginEventHandler.class);
	@Override
	public boolean handleEvent(Event e) {

		// Get the connection that this Handler was called on
		Connection connection = (Connection) e.get("connection");
		AuctionHouse auctionHouse = ((Server) connection.getService()).getAuctionHouse();
		String id = (String) e.get("id");
		boolean loginSuccess = false;

		// Send Login Reply
		Event e1 = new Event("LOGINREPLY");
		e1.put("id", id);
		e1.put("replyTo", e.get("replyTo"));
		if (id == null || id.equals("")) {
			e1.put("response", "blank_id");
			LOG.warn(connection.getService().getServiceName()+ ": Login attempt: blank ID");
		} else {
			if (auctionHouse.addUser((String) e.get("id"))) {
				loginSuccess = true;
				e1.put("response", "success");
				e1.put("items", new ArrayList<Item>(auctionHouse.getItems().values()));

				LOG.info(connection.getService().getServiceName()+": "+ id + " is online");
			} else {
				e1.put("response", "user_exists");
				LOG.warn(connection.getService().getServiceName()+ ": Login attempt: User " + id + " already exists");
			}
		}

		try {
			connection.sendEvent(e1);
		} catch (Exception e3) {
			LOG.error(connection.getService().getServiceName()+ ": Error Sending Event: ", e3);
		}

		// Broadcast events to all clients
		if (loginSuccess) {
			Event e2 = new Event("CLIENTUPDATE");
			e2.put("id", id);
			e2.put("replyTo", e.get("replyTo"));
			e2.put("response", "success");
			e2.put("users", new ArrayList<String>(auctionHouse.getUsers()));

			try {
				LOG.info(connection.getService().getServiceName()+ ": Sending Client Update");
				connection.sendEvent(e2);
			} catch (Exception e3) {
				LOG.error(connection.getService().getServiceName()+ ": Error Sending Event: ", e3);
			}
		}

		return true;
	}
}

package edu.carleton.comp4104.assignment4.server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.RMIRegistry;

public class ServerSpawner {
	private static final Logger LOG = LoggerFactory.getLogger(ServerSpawner.class);
	private static String DEFAULT_cfgFILE = "src/edu/carleton/comp4104/assignment4/server/server.cfg";

	private HashMap<String, Server> servers;
	private AuctionHouse auctionHouse;

	public ServerSpawner(String cfgFile) {

		this.servers = new HashMap<String, Server>();

		// Read the Server cfg file
		BufferedReader br = null;
		try {
			Properties p = new Properties();
			p.load(new FileReader(cfgFile));

			String serverNamesCfg = p.getProperty("SERVERNAMES_CFG");
			String eventsCfg = p.getProperty("EVENTS_CFG");
			String brokerCfg = p.getProperty("BROKER_CFG");
			String auctionHouseClass= p.getProperty("AUCTION_HOUSE");
			if (auctionHouseClass == null)
				auctionHouseClass = "edu.carleton.comp4104.assignment4.server.AuctionHouseSimple";
			
			// Allow configuration of the auction house class
			// This allows for simple sharing of information via
			// the simple class and true fault tolerance when servers are 
			// spawned in separate JVMs.
			Class<?> c = Class.forName(auctionHouseClass);
			auctionHouse = (AuctionHouse)c.newInstance();
			auctionHouse.start();

			String serverName;
			br = new BufferedReader(new FileReader(serverNamesCfg));

			while ((serverName = br.readLine()) != null) {
				Server server = new Server(serverName, eventsCfg, brokerCfg, auctionHouse);
				servers.put(serverName, server);
			}
		} catch (IOException e) {
			LOG.error("", e);
		} catch (ClassNotFoundException e) {
			LOG.error("", e);
		} catch (InstantiationException e) {
			LOG.error("", e);
		} catch (IllegalAccessException e) {
			LOG.error("", e);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				LOG.error("", ex);
			}
		}
	}

	public static void main(String[] args) {

		// Default in case no args
		String cfgFile = DEFAULT_cfgFILE;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-c")) {
				cfgFile = args[++i];
			}
		}

		// Create and Start ServerSpawner
		ServerSpawner serverSpawer = new ServerSpawner(cfgFile);
		serverSpawer.run();
	}

	public void run() {
		for (String key : servers.keySet()) {
			servers.get(key).run();
		}

		// Start Registry for RMI
		RMIRegistry.startInternal();

		System.out.println(servers.size() + " SERVER(s) started.");
	}

}

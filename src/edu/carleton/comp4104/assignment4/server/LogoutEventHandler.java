package edu.carleton.comp4104.assignment4.server;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;

public class LogoutEventHandler implements EventHandler {
	private final Logger LOG = LoggerFactory.getLogger(LogoutEventHandler.class);
	@Override
	public boolean handleEvent(Event e) {

		// Get the connection that this Handler was called on
		Connection connection = (Connection) e.get("connection");
		AuctionHouse auctionHouse = ((Server) connection.getService()).getAuctionHouse();
		String id = (String) e.get("id");

		// Send Login Reply
		Event e1 = new Event("LOGOUTREPLY");
		e1.put("id", id);
		e1.put("replyTo", e.get("replyTo"));
		e1.put("response", "success");
		auctionHouse.removeUser(id);

		LOG.info(connection.getService().getServiceName()+": "+ id + " is now offline.");

		try {
			connection.sendEvent(e1);
		} catch (Exception e3) {
			LOG.error(connection.getService().getServiceName()+ ": Error Sending Event: ", e3);
		}

		// Broadcast events to all clients
		Event e2 = new Event("CLIENTUPDATE");
		e2.put("id", id);
		e2.put("response", "success");
		e2.put("replyTo", e.get("replyTo"));
		e2.put("users", new ArrayList<String>(auctionHouse.getUsers()));

		try {
			connection.sendEvent(e2);
		} catch (Exception e3) {
			LOG.error(connection.getService().getServiceName()+ ": Error Sending Event: ", e3);
		}
		return false;
	}
}

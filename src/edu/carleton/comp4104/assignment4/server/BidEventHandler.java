package edu.carleton.comp4104.assignment4.server;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.EventHandler;
import edu.carleton.comp4104.assignment4.common.Item;

public class BidEventHandler implements EventHandler {
	private final Logger LOG = LoggerFactory.getLogger(BidEventHandler.class);
	@Override
	public boolean handleEvent(Event e) {

		// Get the connection that this Handler was called on
		Connection connection = (Connection) e.get("connection");
		AuctionHouse auctionHouse = ((Server) connection.getService())
				.getAuctionHouse();
		String id = (String) e.get("id");
		String input = (String) e.get("input");
		String itemName = ((String) e.get("itemName"));
		boolean bidSuccess = false;

		// Send Bid Reply
		Event e1 = new Event("BIDREPLY");
		e1.put("id", id);
		e1.put("replyTo", e.get("replyTo"));

		// See if input is valid
		if (validInput(input)) {
			String[] parts = input.split(" ");

			// Bid/Remove on item
			if (parts.length == 1) {

				// Remove item
				if (parts[0].equals("remove")) {
					if (itemName == null) {
						e1.put("response", "no_item_selected_remove");
					} else {
						e1.put("itemName", itemName);
						for (Item item : auctionHouse.getItems().values()) {
							if (item.getItemName().equals(itemName)) {
								if (item.getOwner().equals(id)) {
									auctionHouse.removeItem(itemName);
									e1.put("response", itemName + " removed");
									bidSuccess = true;
								} else {
									e1.put("response", "not_your_item");
								}
								break;
							}
							e1.put("response", "item_does_not_exist");
						}
					}
				}
				// Bid on item
				else {
					if (itemName == null) {
						e1.put("response", "no_item_selected_bid");
					} else {
						float bidAmount = Float.parseFloat(parts[0]);
						if (auctionHouse.updateItem(itemName, bidAmount)) {
							e1.put("itemName", itemName);
							e1.put("response", "bid on " + itemName
									+ " successful!");
							bidSuccess = true;
						} else {
							e1.put("itemName", itemName);
							e1.put("response", "bid_too_low");
						}

					}
				}
			}
			// New item
			else if (parts.length == 3) {

				float bidAmount = Float.parseFloat(parts[2]);
				itemName = parts[1];

				Item newItem = new Item(itemName, id, bidAmount);

				if (auctionHouse.addItem(newItem, itemName)) {
					e1.put("response", itemName + " added to Auction House");
					bidSuccess = true;
				} else {
					e1.put("item", itemName);
					e1.put("response", "item_exists");
				}
			}
		} else {
			e1.put("response", "invalid_input");
		}

		try {
			LOG.info(connection.getService().getServiceName()+": "+ id+ " : " + input);
			connection.sendEvent(e1);
		} catch (Exception e3) {
			LOG.error(connection.getService().getServiceName()+": Error Sending Event: ", e3);
		}

		// Broadcast events to all clients
		if (bidSuccess) {
			Event e2 = new Event("BIDUPDATE");
			e2.put("id", id);
			e2.put("replyTo", e.get("replyTo"));
			e2.put("response", "success");
			e2.put("items", new ArrayList<Item>(auctionHouse.getItems().values()));

			try {
				LOG.info(connection.getService().getServiceName()+": Sending Bid Update");
				connection.sendEvent(e2);
			} catch (Exception e3) {
				LOG.error(connection.getService().getServiceName()+": Error Sending Event: ", e3);
			}
		}
		return true;
	}

	/**
	 * A method that checks if the input is valid
	 * 
	 * @param input
	 * @return
	 */
	public boolean validInput(String input) {

		String[] parts = input.split(" ");
		if (parts.length > 3 || parts.length == 2)
			return false;

		if (parts.length == 1) {
			if (!parts[0].equals("remove")) {
				try {
					Float.parseFloat(parts[0]);
				} catch (NumberFormatException e) {
					return false;
				}
			}
		} else if (parts.length == 3) {
			if (!parts[0].equals("new"))
				return false;
			try {
				Float.parseFloat(parts[2]);
			} catch (NumberFormatException e) {
				return false;
			}
		}

		return true;
	}
}

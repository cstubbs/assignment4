package edu.carleton.comp4104.assignment4.server;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import edu.carleton.comp4104.assignment4.common.Item;

public class AuctionHouseSimple implements AuctionHouse {

	private ConcurrentHashMap<String, Item> items;
	private ConcurrentLinkedQueue<String> users;
	private boolean online;

	public AuctionHouseSimple() {
		this.items = new ConcurrentHashMap<String, Item>();
		this.users = new ConcurrentLinkedQueue<String>();
		this.online = false;
	}

	/**
	 * Adds an Item to the AuctionHouse items if it does not already exist
	 * 
	 * @param newItem
	 * @param itemName
	 * @return true if item was successfully added
	 */
	public synchronized boolean addItem(Item newItem, String itemName) {
		if (!online)
			return false;
		if (items.containsKey(itemName))
			return false;
		items.put(itemName, newItem);
		return true;
	}

	/**
	 * Adds a User to the AuctionHouse users if it does not already exist
	 * 
	 * @param name
	 * @return true if user was successfully added
	 */
	public synchronized boolean addUser(String name) {
		if (!online)
			return false;
		if (!users.contains(name)) {
			users.add(name);
			return true;
		}
		return false;
	}

	public synchronized ConcurrentHashMap<String, Item> getItems() {
		return items;
	}

	public synchronized ConcurrentLinkedQueue<String> getUsers() {
		return users;
	}

	public void start() {
		online = true;
	}
	
	public void stop() {
		online = false;
	}

	@Override
	public boolean removeItem(String itemName) {
		if (online) {
			if (items.containsKey(itemName)) {
				items.remove(itemName);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean removeUser(String name) {
		if (users.contains(name)) {
			users.remove(name);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateItem(String itemName, float value) {
		Item item = items.get(itemName);
		if (item == null)
			return false;
		else if (item.getItemName().equals(itemName) && value > item.getPrice()) {
				item.setPrice(value);
				return true;
		}
		return false;
	}
}

package edu.carleton.comp4104.assignment4.server;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import edu.carleton.comp4104.assignment4.common.Item;

public interface AuctionHouse {
	public boolean addItem(Item newItem, String itemName);
	public boolean addUser(String name);
	public boolean removeItem(String itemName);
	public boolean updateItem(String itemName, float value);
	public boolean removeUser(String name);
	public ConcurrentHashMap<String, Item> getItems();
	public ConcurrentLinkedQueue<String> getUsers();
	public void start();
	public void stop();
}

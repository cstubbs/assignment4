package edu.carleton.comp4104.assignment4.server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Acceptor;
import edu.carleton.comp4104.assignment4.common.Connection;
import edu.carleton.comp4104.assignment4.common.JMSAcceptor;
import edu.carleton.comp4104.assignment4.common.RMIRegistry;
import edu.carleton.comp4104.assignment4.common.Service;

public class Server extends Service {
	private static final Logger LOG = LoggerFactory.getLogger(Server.class);
	private static String DEFAULT_cfgFILE = "src/edu/carleton/comp4104/assignment4/server/server.cfg";
	public static String Publish = "PUBLISH";
	public static String ClientUpdate = "CLIENTUPDATE";
	public static String BidUpdate = "BIDUPDATE";
	public static String DEFAULT_NAME = "SERVER";
	private Acceptor acceptor;
	private AuctionHouse auctionHouse;
	private String serviceName;

	public Server(String serviceName, String eventsCfg, String brokerCfg,
			AuctionHouse auctionHouse) {
		super(serviceName, eventsCfg);

		this.auctionHouse = auctionHouse;
		this.serviceName = serviceName;
		acceptor = new JMSAcceptor(this, brokerCfg);
	}

	public void run() {
		// Start Registry for RMI
		RMIRegistry.startInternal();

		LOG.info(serviceName+" started.");
		try {
			Connection c = acceptor.accept();
			Thread t = new Thread(c);
			t.start();
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	public AuctionHouse getAuctionHouse() {
		return auctionHouse;
	}

	public static void main(String[] args) {
		// Read the Server cfg file
		String cfgFile = DEFAULT_cfgFILE;
		String serverName = DEFAULT_NAME;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-c")) {
				cfgFile = args[++i];
			} else if (args[i].equals("-name")) {
				serverName = args[++i];
			}
		}

		BufferedReader br = null;
		try {
			Properties p = new Properties();
			p.load(new FileReader(cfgFile));

			String eventsCfg = p.getProperty("EVENTS_CFG");
			String brokerCfg = p.getProperty("BROKER_CFG");
			String auctionHouseClass = p.getProperty("AUCTION_HOUSE");
			if (auctionHouseClass == null)
				auctionHouseClass = "edu.carleton.comp4104.assignment4.server.AuctionHouseSimple";

			// Allow configuration of the auction house class
			// This allows for simple sharing of information via
			// the simple class and true fault tolerance when servers are
			// spawned in separate JVMs.
			Class<?> c = Class.forName(auctionHouseClass);
			AuctionHouse auctionHouse = (AuctionHouse) c.newInstance();
		
			// Create a server with a given name and run it with
			// either a simple auction house or one which is fault 
			// tolerant.
			Server server = new Server(serverName, eventsCfg, brokerCfg,
					auctionHouse);
			server.run();
			auctionHouse.start();
		} catch (IOException e) {
			LOG.error("", e);
		} catch (ClassNotFoundException e) {
			LOG.error("", e);
		} catch (InstantiationException e) {
			LOG.error("", e);
		} catch (IllegalAccessException e) {
			LOG.error("", e);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				LOG.error("", ex);
			}
		}
	}
}

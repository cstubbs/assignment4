package edu.carleton.comp4104.assignment4.javaspace;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.client.ClientUpdateEventHandler;
import edu.carleton.comp4104.assignment4.client.RMIHandler;

public class AuctionAgentRMIHandlerImpl extends UnicastRemoteObject implements RMIHandler {
	private final Logger LOG = LoggerFactory.getLogger(ClientUpdateEventHandler.class);
	private static final long serialVersionUID = 1L;
	protected Registry registry;
	protected AuctionAgent auctionAgent;

	public AuctionAgentRMIHandlerImpl(AuctionAgent auctionAgent) throws RemoteException {
		this.auctionAgent = auctionAgent;
	}

	/**
	 * A client attempts a buy from AuctionAgent, gets denied
	 */
	@Override
	public String buyNow(String itemName) {
		LOG.info(auctionAgent.getName()+": "+ itemName + " buy attempt.");
		return itemName + " cannot be bought using this feature, feel free to bid on it though!";
	}

	/**
	 * A client asks for a description of an item owned by an AuctionAgent
	 */
	@Override
	public String getDescription(String itemName) {
		LOG.info(auctionAgent.getName()+": Sent description for " + itemName + ".");
		return "This is the description for " + itemName;
	}
}

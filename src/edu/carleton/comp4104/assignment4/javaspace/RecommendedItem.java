package edu.carleton.comp4104.assignment4.javaspace;

import net.jini.core.entry.Entry;

public class RecommendedItem implements Entry {

	private static final long serialVersionUID = 1L;
	public String name;
	public Double price;
	
	public RecommendedItem() {
		this.name = null;
		this.price = null;
	}
	
	public RecommendedItem(String name) {
		this.name = name;
		this.price = null;
	}
	
	public RecommendedItem(String name, double price) {
		this.name = name;
		this.price = new Double(price);
	}
	
	public String toString() {
		return "RecommendedItem[name: "+name+", price: "+price+"]";
	}
}

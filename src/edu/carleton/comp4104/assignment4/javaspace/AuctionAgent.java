package edu.carleton.comp4104.assignment4.javaspace;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.client.RMIHandler;
import edu.carleton.comp4104.assignment4.common.Broker;
import edu.carleton.comp4104.assignment4.common.ConnectorFactory;
import edu.carleton.comp4104.assignment4.common.Event;
import edu.carleton.comp4104.assignment4.common.JMSAcceptor;
import edu.carleton.comp4104.assignment4.common.JMSConnection;
import edu.carleton.comp4104.assignment4.common.RMIRegistry;

import net.jini.space.JavaSpace;

public class AuctionAgent implements Runnable {
	private static final Logger LOG = LoggerFactory.getLogger(AuctionAgent.class);
	private static String DEFAULT_cfgFILE = "src/edu/carleton/comp4104/assignment4/javaspace/auction-agent.cfg";
	private String name;
	protected ConnectionFactory factory;
	protected javax.jms.Connection connection;
	protected Session session;
	private JMSConnection jmsConnection;	
	private String broker;
	private Registry registry;
	private RMIHandler rmiHandler;
	
	public AuctionAgent(String name) {
		this.name = name;
		Broker.initialize("src/edu/carleton/comp4104/assignment4/common/broker.cfg");
		this.broker = Broker.getBrokerString();
	}
	
	public String getName() {
		return name;
	}
	
	private void init() {
		try {
			
			this.registry = java.rmi.registry.LocateRegistry.getRegistry(RMIRegistry.PORT);
			rmiHandler = new AuctionAgentRMIHandlerImpl(this);
			registry.rebind(name, rmiHandler);
			
			factory = ConnectorFactory.getConnectionFactory(broker);
			connection = factory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
			jmsConnection = new JMSConnection(null, broker, session);
			jmsConnection.createProducer(JMSAcceptor.SERVER_QUEUE);
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (NamingException ne) {
			LOG.error("", ne);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		init();
		
		RecommendedItem template = new RecommendedItem();
		RecommendedItem entry = template;
		JavaSpace space = Locator.getSpace();
		
		while (true) {
			try {
				Thread.sleep(100);
				entry = (RecommendedItem)space.take(template, null, 60*1000);
				if (entry != null) {
					LOG.info(name+" read recommended item from JavaSpace: "+entry);
					final Event e = new Event("BID");
					e.put("itemName", entry.name);
					e.put("input", "new "+entry.name+" "+entry.price);
					e.put("id", name);
					
					jmsConnection.sendEvent(e);
				}
			} catch (Exception e) {
				LOG.error("", e);
			}
		}		
	}
	
	public void stop() {
		try {
			connection.stop();
			session.close();
			jmsConnection.stop();
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (Exception e) {
			LOG.error("", e);
		}
	}
	
	public static void main(String args[]) {
		int numAgents = 1;
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(DEFAULT_cfgFILE));
			numAgents = Integer.parseInt(properties.getProperty("NUM_AGENTS"));
		} catch (FileNotFoundException e) {
			LOG.error("", e);
		} catch (IOException e) {
			LOG.error("", e);
		}
		
		List<AuctionAgent> agentList = new LinkedList<AuctionAgent>();
		for(int i=1; i<=numAgents; i++) {
			AuctionAgent agent = new AuctionAgent("Auction Agent "+i);
			agentList.add(agent);
			Thread agentThread = new Thread(agent);
			agentThread.start();
		}
	}	
}

package edu.carleton.comp4104.assignment4.javaspace;

import net.jini.core.entry.Entry;

public class AuctionItem implements Entry {

	private static final long serialVersionUID = 1L;
	public String name;
	public Double price;
	
	public AuctionItem() {
		this.name = null;
		this.price = null;
	}
	
	public AuctionItem(String name) {
		this.name = name;
		this.price = null;
	}
	
	public AuctionItem(String name, double price) {
		this.name = name;
		this.price = new Double(price);
	}
	
	public String toString() {
		return "AuctionItem[name: "+name+", price: "+price+"]";
	}
}

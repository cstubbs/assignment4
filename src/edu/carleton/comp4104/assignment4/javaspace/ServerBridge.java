package edu.carleton.comp4104.assignment4.javaspace;

import java.io.IOException;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.naming.NamingException;

import net.jini.core.transaction.TransactionException;
import net.jini.space.JavaSpace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.common.Broker;
import edu.carleton.comp4104.assignment4.common.ConnectorFactory;
import edu.carleton.comp4104.assignment4.common.Item;
import edu.carleton.comp4104.assignment4.common.Marshaller;

public class ServerBridge implements MessageListener {
	private final Logger LOG = LoggerFactory.getLogger(ServerBridge.class);
	public static String SERVER_TOPIC = "SERVERAUCTION";
	private ConnectionFactory factory;
	private javax.jms.Connection connection;
	private MessageConsumer topicConsumer;
	private Session session;
	private JavaSpace space;
	private String broker;
	
	public ServerBridge() {
		Broker.initialize("src/edu/carleton/comp4104/assignment4/common/broker.cfg");
		this.broker = Broker.getBrokerString();
	}
	
	public void start() {
		try {
			
			factory = ConnectorFactory.getConnectionFactory(broker);
			connection = factory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
			// Create the topic of interest
			Topic topic = (Topic) session.createTopic(SERVER_TOPIC);
			topicConsumer = session.createConsumer((Destination) topic);
			topicConsumer.setMessageListener(this);
			
			space = Locator.getSpace();
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (NamingException ne) {
			LOG.error("", ne);
		}
	}
	
	public void stop() {
		try {
			connection.stop();
			session.close();
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		}
	}
	
	@Override
	public void onMessage(Message m) {
		try {
			if (m instanceof MapMessage) {
				MapMessage mm = (MapMessage) m;

				if (mm.getString("TYPE").equals("ITEM")) {
					if (mm.getString("OPERATION").equals("ADD")) {
						Item item = (Item) Marshaller.deserializeObject(mm.getBytes("ITEM"));
						AuctionItem auctionItem = new AuctionItem(item.getItemName(), item.getPrice());
						LOG.info("Adding item to JavaSpace: "+auctionItem);
						space.write(auctionItem, null, 20*60*1000);
					}
				}
			}
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (IOException ioE) {
			LOG.error("", ioE);
		} catch (TransactionException e) {
			LOG.error("", e);
		}
	}
	
	public static void main(String args[]) {
		new ServerBridge().start();
	}
}

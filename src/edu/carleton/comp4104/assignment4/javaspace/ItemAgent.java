package edu.carleton.comp4104.assignment4.javaspace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.jini.space.JavaSpace;

public class ItemAgent {
	private static final Logger LOG = LoggerFactory.getLogger(ItemAgent.class);
	// Can probably do away with strings and just use Templates in constructors
	private String item1, item2, recommendItem;
	private boolean doubleItem;
	private AuctionItem template1, template2;
	
	public ItemAgent(String item, String recommendItem) {
		this.item1 = item;
		this.recommendItem = recommendItem;
		doubleItem = false;
		template1 = new AuctionItem(this.item1);
	}
	
	public ItemAgent(String item1, String item2, String recommendItem) {
		this.item1 = item1;
		this.item2 = item2;
		this.recommendItem = recommendItem;
		doubleItem = true;
		template1 = new AuctionItem(this.item1);
		template2 = new AuctionItem(this.item2);
	}
	
	public void run() {
		
		AuctionItem entry = template1;
		JavaSpace space = Locator.getSpace();
		
		while (true) {
			try {
				Thread.sleep(100);
				
				// How long do these need to read for?
				entry = (AuctionItem)space.take(template1, null, 60*1000);
				if (entry != null) {
					if (doubleItem) {
						entry = (AuctionItem)space.take(template2, null, 60*1000);
						if (entry != null)
							space.write(new RecommendedItem(recommendItem, new Double(150)), null, 60*1000);
						
					} else {
						space.write(new RecommendedItem(recommendItem, new Double(150)), null, 60*1000);
					}
				}
			} catch (Exception e) {
				LOG.error("", e);
			}
		}
	}

	public static void main(String args[]) {
		
		// Default in case no args
		//String cfgFile = DEFAULT_cfgFILE;
		String item1 = null;
		String item2 = null;
		String recommendedItem = null;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-i1"))
				item1 = args[++i];
			else if (args[i].equals("-i2"))
				item2 = args[++i];
			else if (args[i].equals("-ri"))
				recommendedItem = args[++i];
		}
			

		// Create and Start Item Agent
		ItemAgent itemAgent;
		
		if (item1 == null || recommendedItem == null) {
			System.out.println("Invalid runtime args, shutting down.");
			return;
		}
		
		if (item2 == null)
			itemAgent = new ItemAgent(item1, recommendedItem);
		else
			itemAgent = new ItemAgent(item1, item2, recommendedItem);
		
		
		itemAgent.run();

	}

}

package edu.carleton.comp4104.assignment4.common;

import java.util.HashMap;

public class Event {
	String type;
	HashMap<String, Object> map;

	public Event(String type) {
		this.type = type;
		this.map = new HashMap<String, Object>();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public HashMap<String, Object> getMap() {
		return map;
	}

	public void setMap(HashMap<String, Object> map) {
		this.map = map;
	}

	public Object get(String key) {
		return map.get(key);
	}

	public void put(String key, Object value) {
		map.put(key, value);
	}
}

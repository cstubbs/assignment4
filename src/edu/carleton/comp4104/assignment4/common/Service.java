package edu.carleton.comp4104.assignment4.common;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Service {

	private static int THREAD_POOL_COUNT = 5;

	protected String serviceName;
	protected Reactor reactor;
	protected ExecutorService threadPool;

	public Service(String serviceName, String eventsCfg) {
		this.serviceName = serviceName;
		this.reactor = new Reactor(eventsCfg);
		this.threadPool = Executors.newFixedThreadPool(THREAD_POOL_COUNT);
	}

	public String getServiceName() {
		return serviceName;
	}

	public Reactor getReactor() {
		return reactor;
	}

	public ExecutorService getThreadPool() {
		return threadPool;
	}
}

package edu.carleton.comp4104.assignment4.common;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.network.NetworkConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Broker {
	private static final Logger LOG = LoggerFactory.getLogger(Broker.class);
	private static String DEFAULT_cfgFILE = "src/edu/carleton/comp4104/assignment4/common/broker.cfg";
	public static int DEFAULT_NUMBROKERS = 3;

	public static int numBrokers;
	public static String BROKER_BASE = "tcp://localhost:6161";

	public static ArrayList<String> BROKERCONNECTS = new ArrayList<String>();
	public static ArrayList<String> BROKERS = new ArrayList<String>();

	public static void main(String[] args) {

		BROKERCONNECTS = new ArrayList<String>();
		BROKERS = new ArrayList<String>();
		
		// Default in case no args
		String cfgFile = DEFAULT_cfgFILE;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-c")) {
				cfgFile = args[++i];
			}
		}
		setupBroker(cfgFile);
		try {
			@SuppressWarnings("unused")
			ArrayList<BrokerService> brokerServices = startBrokers();
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	public static BrokerService startBroker(String name, String address, String networkConnector) throws Exception {
		BrokerService broker = new BrokerService();
		if (name != null)
			broker.setBrokerName(name);
		broker.setUseJmx(true);
		broker.addConnector(address);
		if (networkConnector != null) {
			NetworkConnector n = broker.addNetworkConnector(networkConnector);
			n.setDuplex(true);
		}
		broker.start();
		return broker;
	}

	/**
	 * Setup the potential BROKERS that can be connected to from NUM_BROKERS in
	 * cfg file
	 * 
	 * @param cfgFile
	 */
	private static void setupBroker(String cfgFile) {
		try {
			Properties p = new Properties();
			p.load(new FileReader(cfgFile));

			numBrokers = Integer.parseInt(p.getProperty("NUM_BROKERS"));

			generateBrokerNames();
			generateConnectArray();

		} catch (FileNotFoundException e) {
			LOG.error("The cfgFile not found.");
		} catch (IOException e) {
			LOG.error("", e);
		}
	}
	
	/**
	 * Create Broker names
	 */
	private static void generateBrokerNames() {
		for (int i = 0; i < numBrokers; i++) {
			BROKERS.add(BROKER_BASE + i);
		}
	}
	
	/**
	 * Create failover strings that will be connected to
	 */
	private static void generateConnectArray() {
		for (int i = 0; i < numBrokers; i++) {
			String str = new String("failover:(" + getFailoverString(i) + ")?timeout=1000&randomize=false");
			BROKERCONNECTS.add(str);
		}
	}
	
	/**
	 * Helper to create failover string
	 */
	private static String getFailoverString(int index) {
		String result = BROKERS.get(index) + ",";
		ArrayList<String> temp = new ArrayList<String>();
		temp.addAll(BROKERS);
		temp.remove(BROKERS.get(index));

		for (int i = 0; i < numBrokers - 1; i++) {
			Random randIndex = new Random();
			int y = Math.abs(randIndex.nextInt()) % temp.size();
			result += temp.get(y);
			temp.remove(y);

			if (i != numBrokers - 2) {
				result += ",";
			}
		}
		return result;
	}

	private static ArrayList<BrokerService> startBrokers() throws Exception {
		ArrayList<BrokerService> toReturn = new ArrayList<BrokerService>();
		for (int i = 0; i < numBrokers; i++) {
			String networkConnector = getNetworkConnector(i);
			toReturn.add(startBroker("BROKER" + i, BROKERS.get(i), networkConnector));
		}

		return toReturn;
	}

	private static String getNetworkConnector(int index) {
		if (index == numBrokers - 1) {
			return null;
		}
		String result = "static:(";
		for (int i = index + 1; i < numBrokers; i++) {
			if (i < BROKERS.size()) {
				result += BROKERS.get(i);
				if (i != numBrokers - 1) {
					result += ",";
				}
			}
		}
		result += ")";
		return result;
	}

	public static String getBrokerString() {
		Random r = new Random();
		return BROKERCONNECTS.get(Math.abs(r.nextInt()) % BROKERS.size());
	}

	public static void initialize(String cfgFile) {
		setupBroker(cfgFile);
	}
}

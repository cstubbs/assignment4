package edu.carleton.comp4104.assignment4.common;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JMSConnector extends Connector {
	private static final Logger LOG = LoggerFactory.getLogger(JMSConnector.class);
	protected ConnectionFactory factory;
	protected javax.jms.Connection connection;
	protected Session session;
	private String broker;

	public JMSConnector(Service service, String brokerCfgFile) {
		super(service);

		Broker.initialize(brokerCfgFile);
		this.broker = Broker.getBrokerString();
	}

	public void init() {
		try {
			factory = ConnectorFactory.getConnectionFactory(broker);
			connection = factory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (NamingException ne) {
			LOG.error("", ne);
		}
	}

	@Override
	public Connection connect() {
		init();

		JMSConnection c = new JMSConnection(service, broker, session);
		// Assuming that this name is unique
		c.createConsumer(service.getServiceName());
		c.createProducer(JMSAcceptor.SERVER_QUEUE);
		c.subscribeToUpdateTopics();

		return c;
	}

	@Override
	public void stop() throws Exception {
		session = null;
		connection.stop();
		connection.close();
		connection = null;
		factory = null;
	}

}

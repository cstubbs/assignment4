package edu.carleton.comp4104.assignment4.common;

import java.io.Serializable;
import java.text.DecimalFormat;

public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String itemName;
	private String owner;
	private float price;

	public Item(String itemName, String owner, float price) {
		this.itemName = itemName;
		this.owner = owner;
		this.price = price;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.00");
		return itemName + ": $" + df.format(price) + " for auction by " + owner;
	}
}

package edu.carleton.comp4104.assignment4.common;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JMSAcceptor extends Acceptor {
	private static final Logger LOG = LoggerFactory.getLogger(JMSAcceptor.class);
	public static String SERVER_QUEUE = "SERVER_QUEUE";
	private ConnectionFactory factory;
	private javax.jms.Connection connection;
	private Session session;
	private String broker;

	public JMSAcceptor(Service service, String brokerCfgFile) {
		super(service);

		Broker.initialize(brokerCfgFile);
		this.broker = Broker.getBrokerString();
	}

	private void connect() {
		try {
			factory = ConnectorFactory.getConnectionFactory(broker);
			connection = factory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE); // false=NotTransacted
		} catch (JMSException jmsE) {
			LOG.error("", jmsE);
		} catch (NamingException ne) {
			LOG.error("", ne);
		}
	}

	public Connection accept() {
		// Create session for a default well known broker and read forever
		connect();

		JMSConnection c = new JMSConnection(service, broker, session);
		c.createConsumer(JMSAcceptor.SERVER_QUEUE);

		return c;
	}

	@Override
	public void stop() throws Exception {
		session = null;
		connection.stop();
		connection = null;
		factory = null;
	}
}

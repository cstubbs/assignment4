package edu.carleton.comp4104.assignment4.common;

public abstract class Acceptor {

	protected Service service;

	public Acceptor(Service service) {
		this.service = service;
	}

	public abstract Connection accept() throws Exception;

	public abstract void stop() throws Exception;
}

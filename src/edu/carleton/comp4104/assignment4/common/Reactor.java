package edu.carleton.comp4104.assignment4.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reactor {
	private final Logger LOG = LoggerFactory.getLogger(Reactor.class);
	private static String DEFAULT_FILE = "src/edu/carleton/comp4104/networking/T05Reactor/common/services.cfg";
	private HashMap<String, EventHandler> map;
	private Properties properties;

	public Reactor(String eventsFile) {
		map = new HashMap<String, EventHandler>();
		properties = new Properties();
		try {
			properties.load(new FileInputStream(eventsFile));
			init();
		} catch (FileNotFoundException e) {
			LOG.error("File not found." + eventsFile);
			System.exit(0);
		} catch (IOException e) {
			LOG.error("", e);
			System.exit(0);
		} catch (ClassNotFoundException e) {
			LOG.error("", e);
			System.exit(0);
		} catch (InstantiationException e) {
			LOG.error("", e);
			System.exit(0);
		} catch (IllegalAccessException e) {
			LOG.error("", e);
			System.exit(0);
		}
	}

	public Reactor() {
		this(DEFAULT_FILE);
	}

	/**
	 * Initialize the possible events, expecting a set of key-value pairs.
	 * 
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public void init() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		String className;
		for (Object key : properties.keySet()) {
			className = (String) properties.get(key);
			@SuppressWarnings("unchecked")
			Class<EventHandler> clazz = (Class<EventHandler>) Class.forName(className);
			EventHandler h = clazz.newInstance();
			registerHandler((String) key, h);
		}
	}

	public void registerHandler(String type, EventHandler handler) {
		map.put(type, handler);
	}

	public void removeHandler(String type) {
		map.remove(type);
	}

	/**
	 * Dispatches Event e to the appropriate EventHandler.
	 * 
	 * @param e
	 * @return
	 */
	public boolean dispatch(Event e) {
		EventHandler h = map.get(e.getType());
		if (h == null)
			h = map.get("UNKNOWN");
		return h.handleEvent(e);
	}
}

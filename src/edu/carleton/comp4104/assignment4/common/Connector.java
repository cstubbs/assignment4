package edu.carleton.comp4104.assignment4.common;

public abstract class Connector {

	protected Service service;

	public Connector(Service service) {
		this.service = service;
	}

	public abstract Connection connect() throws Exception;

	public abstract void stop() throws Exception;
}

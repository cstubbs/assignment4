package edu.carleton.comp4104.assignment4.common;

import java.util.Enumeration;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.carleton.comp4104.assignment4.server.Server;

public class JMSConnection extends Connection {
	private static final Logger LOG = LoggerFactory.getLogger(JMSConnection.class);
	protected String broker;
	protected MessageConsumer consumer;
	protected MessageProducer producer;
	protected Queue produceQueue;
	protected Queue consumeQueue;
	protected Session session;
	protected Topic publishTopic;
	protected Topic clientUpdateTopic;
	protected Topic bidUpdateTopic;
	protected ConsumerListenerRunnable publishRunnable;
	protected ConsumerListenerRunnable clientUpdateRunnable;
	protected ConsumerListenerRunnable bidUpdateRunnable;

	public JMSConnection(Service service, String broker, Session session) {
		super(service);

		this.broker = broker;
		this.session = session;

		try {
			publishTopic = session.createTopic(Server.Publish);
			clientUpdateTopic = session.createTopic(Server.ClientUpdate);
			bidUpdateTopic = session.createTopic(Server.BidUpdate);
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	@Override
	public Event getEvent() throws Exception {
		MapMessage m = (MapMessage) consumer.receive();
		return messageToEvent(m);
	}

	@Override
	public void sendEvent(Event e) throws Exception {
		MapMessage m = session.createMapMessage();
		m.setJMSReplyTo(consumeQueue);
		m.setJMSType((String) e.getType());

		for (String key : e.getMap().keySet()) {
			m.setBytes(key, Marshaller.serializeObject(e.get(key)));
		}

		// If the event being sent is a publish event, then broadcast it to the
		// topics
		if (m.getJMSType().equals("PUBLISH")) {
			publishMessage(m, publishTopic);
		} else if (m.getJMSType().equals("CLIENTUPDATE")) {
			publishMessage(m, clientUpdateTopic);
		} else if (m.getJMSType().equals("BIDUPDATE")) {
			publishMessage(m, bidUpdateTopic);
		} else {
			if (producer != null) {
				producer.send(m);
			} else {
				// If there isn't a specific produce queue set, then look at the
				// replyTo value of the event
				// to figure out where to send the message
				MessageProducer producerTemp = session.createProducer((Destination) e.get("replyTo"));
				producerTemp.send(m);
			}
		}
	}

	@Override
	public void stop() throws Exception {
		// Clean up Runnables
		publishRunnable.stop();
		clientUpdateRunnable.stop();
		bidUpdateRunnable.stop();
		publishRunnable = null;
		clientUpdateRunnable = null;
		bidUpdateRunnable = null;

		running = false;
	}

	public void createConsumer(String consumer) {
		try {
			consumeQueue = session.createQueue(consumer);
			this.consumer = session.createConsumer(consumeQueue);
		} catch (JMSException jms) {
			LOG.error("", jms);
		}
	}

	public void createProducer(String producer) {
		try {
			produceQueue = session.createQueue(producer);
			this.producer = session.createProducer(produceQueue);
		} catch (JMSException jms) {
			LOG.error("", jms);
		}
	}

	public void subscribeToUpdateTopics() {

		// Adding listener to the publish topic
		// Could paramaterize the topic string and create the topic inside
		try {
			MessageConsumer publishTopicConsumer = session.createConsumer(publishTopic);
			publishRunnable = new ConsumerListenerRunnable(publishTopicConsumer, service, this);
			Thread t = new Thread(publishRunnable);
			t.start();

			MessageConsumer clientUpdateTopicConsumer = session.createConsumer(clientUpdateTopic);
			clientUpdateRunnable = new ConsumerListenerRunnable(clientUpdateTopicConsumer, service, this);
			t = new Thread(clientUpdateRunnable);
			t.start();

			MessageConsumer bidUpdateTopicConsumer = session.createConsumer(bidUpdateTopic);
			bidUpdateRunnable = new ConsumerListenerRunnable(bidUpdateTopicConsumer, service, this);
			t = new Thread(bidUpdateRunnable);
			t.start();
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	public void publishMessage(MapMessage m, Topic t) {
		try {
			MessageProducer publishProducer = session.createProducer(t);
			publishProducer.send(m);
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	@SuppressWarnings("unchecked")
	public Event messageToEvent(MapMessage m) {

		try {
			Event e = new Event(m.getJMSType());

			Enumeration<String> mapNames = (Enumeration<String>) m.getMapNames();

			while (mapNames.hasMoreElements()) {
				String pName = (String) mapNames.nextElement();
				e.put(pName, Marshaller.deserializeObject(m.getBytes(pName)));
			}

			e.put("replyTo", m.getJMSReplyTo());
			e.put("connection", this);
			e.put("service", service);

			return e;
		} catch (Exception e) {

		}

		return null;
	}
}

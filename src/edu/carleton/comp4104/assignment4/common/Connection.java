package edu.carleton.comp4104.assignment4.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Connection implements Runnable {
	private static final Logger LOG = LoggerFactory.getLogger(Connection.class);
	protected Service service;
	protected boolean running;

	public Connection(Service service) {
		this.service = service;
		this.running = false;
	}

	public abstract Event getEvent() throws Exception;

	public abstract void sendEvent(Event e) throws Exception;

	public abstract void stop() throws Exception;

	@Override
	public void run() {
		running = true;
		try {
			while (running) {
				service.getReactor().dispatch(getEvent());
			}
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	public Service getService() {
		return service;
	}
}

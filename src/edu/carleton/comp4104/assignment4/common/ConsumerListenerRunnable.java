package edu.carleton.comp4104.assignment4.common;

import javax.jms.MapMessage;
import javax.jms.MessageConsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerListenerRunnable implements Runnable {
	private static final Logger LOG = LoggerFactory.getLogger(ConsumerListenerRunnable.class);
	protected Service service;
	protected JMSConnection connection;
	protected MessageConsumer consumer;
	protected boolean running;

	public ConsumerListenerRunnable(MessageConsumer consumer, Service service, JMSConnection connection) {
		this.service = service;
		this.connection = connection;
		this.consumer = consumer;
		this.running = false;
	}

	public void run() {
		running = true;
		try {
			while (running) {
				MapMessage m = (MapMessage) consumer.receive();
				Event e = connection.messageToEvent(m);
				if (e != null) {
					service.getReactor().dispatch(e);
				}
			}
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	public void stop() {
		running = false;
	}
}
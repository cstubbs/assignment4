package edu.carleton.comp4104.assignment4.common;

public interface EventHandler {
	public boolean handleEvent(Event e);
}
